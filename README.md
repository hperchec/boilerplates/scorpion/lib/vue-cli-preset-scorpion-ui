# Scorpion UI preset for vue-cli

Run **vue-cli** `create` command (where `<app_name>` is your project name):

```sh
vue create -b --preset direct:https://gitlab.com/hperchec/boilerplates/scorpion/lib/vue-cli-preset-scorpion-ui#main -c <app_name>
```

## Development

For local tests run:

```sh
vue create -b --preset ./vue-cli-preset-scorpion-ui <app_name>
```

----

Made with ❤ by [Hervé Perchec](http://herve-perchec.com/)